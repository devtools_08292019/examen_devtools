# examen_recetas_devops

Este repositorio contiene el proyecto que se utilizara para el examen de cierre del curso de Devops tools

# Instrucciones para Evaluación

* Se tomarán en cuenta los mensajes de los commits sean descriptivos.

* Que se incluyan todos los branches y commits dentro del repositorio.

1. Clona este repositorio.

2. Crea una branch llamada "cambios_<tu_nombre>".

3. Si no existe una carpeta para el Estado donde tu naciste, favor de crearla.

4. Entra a la carpeta que corresponda al Estado donde naciste.

5. Agrega una receta típica de tu Estado.

6. Haz un commit.

7. Cambia el nombre del archivo en el que pusiste la receta.

8. Crea un nuevo commit.

9. Haz push al repositorio de estos cambios.

10. Crea un Pull Request a master usando Bitbucket.

11. Crea una branch en tu repositorio local que parta del primer commit, cuyo nombre sea igual que el branch interior agregando "_2"

12. Busca en otro Estado una receta que sea de tu agrado y muevela a la carpeta de tu Estado.

13. Haz un commit para este cambio.

14. Para que no sea tan fácil identificar tal acto de piratería, modifica la receta.

15. Crea un nuevo commit.

16. Crea una nueva branch agregando "_3" al nombre, que apunte al commit en el que moviste la receta del directorio.

17. Cambia el nombre de la receta.

18. Haz un commit.

19. Combina las dos branch creando un nuevo commit de unión.

20. Abre el log modificado para ver una sola línea de cada commit y ver la estructura gráfica del repositorio.

21. Toma una captura de pantalla de tu bitácora.

22. Crea una branch agregando "_4" al nombre

22. Pegala en la carpeta raíz del repositorio.

23. Crea un commit.

24. Regresa a tu branch con terminación "_3".

25. Cancela el commit de unión que habías creado.

26. Haz rebase con tu branch con terminación "_2".

27. Haz push a las 3 branches que usaste en este ejercicio.